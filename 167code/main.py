from fibo import fib, fib2
import sys
import json
import os
import glob
import random
from urllib.request import urlopen
import reprlib
'''
for n in range(2, 10):
    for x in range(2, n):
        if n % x == 0:
            print(n, '=', x, '*', n//x)
            break
    else:
        print(n, '是一个素数')

for i in range(2,100):
    for y in range(2,i):
        if i % y == 0:
            print(i, '=',y,'*',i//y)


number = {
    "2150810124": "167",
    "2150810123": "js",
    "111": "11"
}

# print(number["2150810124"])
del number["111"]
# print(list(number))
number['777777'] = '777'
# print(list (number))

for k in number.items():
    print(k)
print()
for k, v in number.items():
    print(k, v)
print()
for f in sorted(set(number)):
    print(f)
print()

questions = ['name', 'quest', 'favorite color']
answers = ['lancelot', 'the holy grail', 'blue']
for q, a in zip(questions, answers):
    print('What is your {0}?  It is {1}.'.format(q, a))
print()

if __name__ == "__main__":
    fib(1000)
    print(fib2(2000))
    print(sys.path)

'''

'''
s = 'wdnmd!'
print(str(s))
print(repr(s))
'''

'''
if __name__ == "__main__":
    f = open("a.json", 'w')
    info = {
        "lllll": 'jjjjjj',
        'ssssss': 'kkkkk',
        'sad232': (334, 'd2', 34222),
    }
    json.dump(info, f)
    f.close()

    with open("a.json", 'r') as f:
        info = json.loads(f.read())
    for k, v in info.items():
        print(k, v)
'''
'''
if __name__ == "__main__":
    os.system('start http:www.bilibili.com')
'''
'''
print(glob.glob('*.py'))
print(random.random())
'''


with urlopen('http://www.bilibili.com') as response:
    for line in response:
        line = line.decode('utf-8')  # Decoding the binary data to text.
        print(line)


'''
print(reprlib.repr(set('supercalifragilisticexpialidocious')))
'''
